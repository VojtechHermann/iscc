//
//  CGSizeExtension.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

extension CGSize {
    static func >= (lhs: CGSize, rhs: CGSize) -> Bool {
        return lhs.height >= rhs.height && lhs.width >= rhs.width
    }
}
