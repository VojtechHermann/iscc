//
//  LocaleExtension.swift
//  StreamCinema.atv
//
//  Created by SCC on 25/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
public extension Locale {

    private static let allIso639_2LanguageIdentifiers: [String: String] = {
        guard let path = Bundle.main.path(forResource: "iso639_1_to_iso639_2", ofType: "plist") else { return [:] }
        guard let result = NSDictionary(contentsOfFile: path) as? [String: String] else { return [:] }

        return result
    }()

    var iso639_2LanguageCode: String? {
        guard let languageCode = languageCode else { return nil }
        return Locale.allIso639_2LanguageIdentifiers[languageCode]
    }

}
