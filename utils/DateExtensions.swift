//
//  DateExtensions.swift
//  StreamCinema.atv
//
//  Created by SCC on 03/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

extension Date {
    var toTime: String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH':'mm"
        return dateFormatter.string(from: self)
    }
    
    var toDateString: String {
        let dateFormatter = DateFormatter() //2020-08-14
        dateFormatter.dateFormat = "YYYY'-'MM'-'dd"
        return dateFormatter.string(from: self)
    }
}

//public enum TimeType {
//    case min
//    case hour
//    case minOrHour
//    case millisecond
//}
//
//extension TimeInterval {
//    public func toString(type: TimeType) -> String {
//        var second = ceil(self)
//        var min = floor(second / 60)
//        second -= min * 60
//        switch type {
//        case .min:
//            return String(format: "%02.0f:%02.0f", min, second)
//        case .hour:
//            let hour = floor(min / 60)
//            min -= hour * 60
//            return String(format: "%.0f:%02.0f:%02.0f", hour, min, second)
//        case .minOrHour:
//            let hour = floor(min / 60)
//            if hour > 0 {
//                min -= hour * 60
//                return String(format: "%.0f:%02.0f:%02.0f", hour, min, second)
//            } else {
//                return String(format: "%02.0f:%02.0f", min, second)
//            }
//        case .millisecond:
//            var time = Int(self * 100)
//            let millisecond = time % 100
//            time /= 100
//            let sec = time % 60
//            time /= 60
//            let min = time % 60
//            time /= 60
//            let hour = time % 60
//            if hour > 0 {
//                return String(format: "%d:%02d:%02d.%02d", hour, min, sec, millisecond)
//            } else {
//                return String(format: "%02d:%02d.%02d", min, sec, millisecond)
//            }
//        }
//    }
//}
