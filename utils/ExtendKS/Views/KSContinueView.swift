//
//  ContinueView.swift
//  StreamCinema.atv
//
//  Created by SCC on 15/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class KSContinueView: UIView {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var countDownLabel: UILabel!
    
    public func set(countDown value: String) {
        self.countDownLabel.text = value
    }
    
    public func set(poster: String?) {
        if let posterString = poster,
            let posterURL = URL(string: posterString) {
            self.imageView.setCashedImage(url: posterURL, type: .emptyLoading)
        }
    }
    
    public func set(title: String?) {
        self.titleLabel.text = title
        self.titleLabel.layoutIfNeeded()
    }
}
