//
//  PositionController.swift
//  Pods-Demo
//
//  Created by Jérémy Marchand on 04/03/2018.
//

import Foundation

protocol KSPositionController: class {
    var isEnabled: Bool { get set }

    func click(_ sender: KSLongPressGestureRecogniser)
    func playOrPause(_ sender: Any)
}
