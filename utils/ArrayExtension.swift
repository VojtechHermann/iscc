//
//  ArrayExtension.swift
//  StreamCinema.atv
//
//  Created by SCC on 21/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
            var results = [Element]()

            forEach { (element) in
                let existingElements = results.filter {
                    return includeElement(element, $0)
                }
                if existingElements.count == 0 {
                    results.append(element)
                }
            }

            return results
        }
}

public extension Sequence {
    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
            var results = [Element]()

            forEach { (element) in
                let existingElements = results.filter {
                    return includeElement(element, $0)
                }
                if existingElements.count == 0 {
                    results.append(element)
                }
            }

            return results
        
    }
}


