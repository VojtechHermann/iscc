//
//  LoginModel.swift
//  StreamCinema
//
//  Created by SCC on 23/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import XMLMapper

struct WSTokenData {
    var token: String?
    var passwordHash: String?
    var name: String?
}

final class ResultEmptyDataModel:XMLMappable {
    var nodeName: String!
    var status: String?
    
    func mapping(map: XMLMap) {
        self.status <- map["status"]
    }
    required init(map: XMLMap) {

    }
}

final class LoginModel:XMLMappable {
    var nodeName: String!

    var status: String?
    var token: String?
    required init(map: XMLMap) {

    }

    func mapping(map: XMLMap) {
        self.token <- map["token"]
        self.status <- map["status"]
    }
}

final class SaltModel:XMLMappable {
    var nodeName: String!

    var salt: String?
    var status: String?
    required init(map: XMLMap) {

    }

    func mapping(map: XMLMap) {
        self.salt <- map["salt"]
        self.status <- map["status"]
    }
}

final class UserModel:XMLMappable {
    var nodeName: String!
    
    var status:String?
    var id: String?
    var ident: String?
    var username: String?
    var email: String?
    var points: String?
    var files: String?
    var bytes: String?
    var score_files: String?
    var score_bytes: String?
    var private_files: String?
    var private_bytes: String?
    var private_space: String?
    var tester: String?
    var vip: String?
    var vip_days: String?
    var vip_hours: String?
    var vip_minutes: String?
    var vip_until: String?
    var terms_version: String?
    var email_verified: String?
    
    required init(map: XMLMap) {

    }

    func mapping(map: XMLMap) {
        self.status <- map["status"]
        self.id <- map["id"]
        self.ident <- map["ident"]
        self.username <- map["username"]
        self.email <- map["email"]
        self.points <- map["points"]
        self.files <- map["files"]
        self.bytes <- map["bytes"]
        self.score_files <- map["score_files"]
        self.score_bytes <- map["score_bytes"]
        self.private_files <- map["private_files"]
        self.private_bytes <- map["private_bytes"]
        self.private_space <- map["private_space"]
        self.tester <- map["tester"]
        self.vip_days <- map["vip_days"]
        self.vip_hours <- map["vip_hours"]
        self.vip_minutes <- map["vip_minutes"]
        self.vip_until <- map["vip_until"]
        self.terms_version <- map["terms_version"]
        self.email_verified <- map["email_verified"]
        
    }
}
