//
//  SC2_ApiRequest.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Alamofire
import Moya

extension Provider {
    static let gitRequest: MoyaProvider<GitRequest> = NetworkingClient.provider()
}
//<key>com.apple.developer.team-identifier</key><string>5PUJGSG8GM</string>
//https://gitlab.com/api/v4/projects/21628497/releases
enum GitRequest: TargetType {
    case getVersion

    
    var baseURL: URL {
        return URLSettings.gitUrl
    }
    
    var method: Moya.Method {
        switch self {
        case .getVersion:
            return .get
        }
    }
    
    var headers: [String: String]? {
        return ["PRIVATE-TOKEN":"xoYqQd64FUxPyykS4jRj"]
    }
    
    var path: String {
        switch self {
        case .getVersion:
            return self.projectReleasePath
        }
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return URLEncoding.default
    }
    
    var task: Task {
        switch self {
        case .getVersion:
            return .requestPlain 
        }
    }
    
    var sampleData: Data {
        return Data()
    }
}

extension GitRequest {
    var projectReleasePath: String {
        return "projects/21628497/releases"
    }
}
