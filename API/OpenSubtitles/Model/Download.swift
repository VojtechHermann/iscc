//
//  Download.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct OSDownload: Model {
    let link: String?
    let fname: String?
    let requests, allowed, remaining: Int?
    let message: String?
    var added:Bool = false
    var langName:String?
    
    enum CodingKeys: String, CodingKey {
        case link
        case fname = "file_name"
        case requests, allowed, remaining, message
    }
        
    static func empty<T>() -> T where T: Model {
        let download = OSDownload(link: nil, fname: nil, requests: nil, allowed: nil, remaining: nil, message: nil)
        guard let empytDownload = download as?T else {
            fatalError()
        }
        return empytDownload
    }
}
