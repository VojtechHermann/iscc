//
//  OSService.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import Combine


final class OSService {

    enum OSError: Error {
        case noImdbID
        case emptyData
        case lessLanguagesThanExpeced
    }

    enum Language: String, CaseIterable {
        case cz = "cs"
        case sk = "sk"
        case en = "en"

        var identifiers: String {
            switch self {
            case .sk: return "_sk_slk_slo_slk"
            case .cz: return "_cs_ces_cze_ces"
            case .en: return "_en_eng_eng_eng_ang"
            }
        }
    }


   let apiClient = Provider.osRequest

    
    public func login(name:String, pass:String) -> AnyPublisher<OSLoginResult, Error> {
        apiClient
            .request(request: OSRequest.login(name: name, pass: pass), model: OSLoginResult.self)

    }
    
    public func find(imdbid: String, lang: [String]) -> AnyPublisher<[OSDownload], Error> {

        guard let imdbID = imdbid.split(separator: "t").last else {
            return Fail(error: OSError.noImdbID).eraseToAnyPublisher()
        }

        let allData = apiClient.request(request: OSRequest.findWith(imdbid: String(imdbID), lang: lang), model: OSFindResult.self)
            .map {
                $0.data.sorted { (lhas, rhs) -> Bool in
                    if let lhsRating = rhs.attributes?.ratings,
                       let rhsRating = rhs.attributes?.ratings {
                        return lhsRating > rhsRating
                    }
                    return false
                }
        }.share()



        let nevim: [AnyPublisher<OSDownload,Error>] = Language.allCases
            .map { (language) -> AnyPublisher<OSDownload, Error> in
                return allData
                    .map { $0.filter { $0.attributes?.language?.lowercased() == language.rawValue } }
                    .flatMap { [weak self] (languageData) -> AnyPublisher<OSDownload, Error> in
                        guard let `self` = self else { return Fail(error: OSError.emptyData).eraseToAnyPublisher() }
                        return self.getLinkFrom(languageData)
                    }
                    .map { link -> OSDownload in
                        var copyLink = link
                        copyLink.langName = "\(link.fname ?? imdbid)\(language.identifiers)"
                        return copyLink
                    }
                    .eraseToAnyPublisher()
            }

        guard let firstRequest = nevim[safe: 0],
              let secondReqest = nevim[safe: 1],
              let thirdRequest = nevim[safe: 2] else { return Fail(error: OSError.lessLanguagesThanExpeced).eraseToAnyPublisher() }

        // Zip waits on value from each publisher.
         return Publishers.Zip3.init(firstRequest, secondReqest, thirdRequest)
                    .map { firstLink, secondLink, thirdLink -> [OSDownload] in
                        return [firstLink, secondLink, thirdLink]
                    }.eraseToAnyPublisher()
    }
    
    private func getLinkFrom(_ allData: [OSData]) -> AnyPublisher<OSDownload, Error> {

        if let data = allData.first,
           let id = data.attributes?.files?.first?.id,
           let name = data.attributes?.files?.first?.fileName {
                return getDownload(id: String(id), name: name)
        } else {
            return Fail(error: OSError.emptyData).eraseToAnyPublisher()
        }
    }
    
    public func getDownload(id:String, name:String) -> AnyPublisher<OSDownload, Error> {
        apiClient.request(request: OSRequest.download(id: id, name: name), model: OSDownload.self)
    }

//    public func downloadFile(from url:URL, completion: @escaping (Result<OSProgressModel, RequestErrors>) -> Void) {
//
//    }
}
