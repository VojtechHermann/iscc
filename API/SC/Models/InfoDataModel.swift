//
//  InfoDataModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

// MARK: - InfoData
public struct InfoData: DataConvertible, Hashable, Model {
    let index, type, id: String?
    let score: Double?
    let source: SourceInfo?
    let tvInfo: TvInfo?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case score = "_score"
        case source = "_source"
        case tvInfo = "tv_info"
        case index = "_index"
        case type = "_type"
    }
    
    static func empty<T>() -> T where T: Model {
        let infoData = InfoData(index: nil, type: nil, id: "", score: 0.0, source: SourceInfo.empty(), tvInfo: nil)
        guard let emptyInfoData = infoData as?T else {
            fatalError()
        }
        return emptyInfoData
    }
}

// MARK: - TvInfo
public struct TvInfo: Hashable, Model {
    let station: TVStation?
    let date, end, csfdID: String?

    enum CodingKeys: String, CodingKey {
        case station, date, end
        case csfdID
    }
    
    static func empty<T>() -> T where T: Model {
        let tvInfo = TvInfo(station: nil, date: nil, end: nil, csfdID: nil)
        guard let emptySource = tvInfo as?T else {
            fatalError()
        }
        return emptySource
    }
}

// MARK: - Station
public struct TVStation: Hashable, Model {
    let name, logo: String?
    
    func getLogoURL() -> URL? {
        guard let logoUrlString = self.logo else { return nil }
        return URL(string: logoUrlString)
    }
    static func empty<T>() -> T where T: Model {
        let tvInfo = TVStation(name: nil, logo: nil)
        guard let emptySource = tvInfo as?T else {
            fatalError()
        }
        return emptySource
    }
}
