//
//  TabBarScreenView.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI

struct TabBarScreenView: View {
    var body: some View {

        TabBarTemplateView {

            EmptyView()

        }
        .ignoresSafeArea(.container, edges: .all)
    }
}


struct TabBarScreenView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarScreenView()
    }
}
