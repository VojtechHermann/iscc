//
//  InfoCell.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit


final class InfoCell: SettingsCell {
    private let title: UILabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }

    private func setupView() {
        self.addSubview(self.title)
        self.title.translatesAutoresizingMaskIntoConstraints = false
        self.title.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.title.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
    }

    public func set(text:String) {
        self.title.text = text
    }

    static func getCell(_ tableView: UITableView) -> InfoCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell") as? InfoCell {
            return cell
        }
        return InfoCell(style: .default, reuseIdentifier: "InfoCell")
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        if context.nextFocusedView === self {
            coordinator.addCoordinatedAnimations({
                self.title.textColor = .lightGray
            }, completion: nil)
        }
        else {
            coordinator.addCoordinatedAnimations({
                self.title.textColor = .label
            }, completion: nil)
        }
    }
    
}
