//
//  LoginViewModel.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 21.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import Combine

final class LoginViewModel {

    // MARK: Public props
    let appData: AppData
    let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()

    // MARK: - Private props
    private var cancelables: Set<AnyCancellable> = Set()

    init(appData: AppData) {
        self.appData = appData
    }


}
