//
//  MoviesCollectionCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 19/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import TraktKit

public enum TraktCellType: Int, CaseIterable {
    case watchList = 0
    case tvShowWatchList = 1
    case movieHistory = 2
    case showHistory = 3
    case meLists = 4
    case meFollowers = 5
    
    case unowned = 1000
    
    var title:String {
        switch self {
        case .watchList:
            return String(localized: .movies) + " " + String(localized: .watch_list)
        case .tvShowWatchList:
            return String(localized: .tvShows) + " " + String(localized: .watch_list)
        case .movieHistory:
            return String(localized: .movies) + " " + String(localized: .watch_history)
        case .showHistory:
            return String(localized: .tvShows) + " " + String(localized: .watch_history)
        case .meLists:
            return String(localized: .meList)
        case .meFollowers:
            return String(localized: .meFriends)
        case .unowned:
            return ""
        }
    }
}

public protocol MoviesCollectionCellDelegate: AnyObject {
    func moviesCollectionCell(_ cell:MoviesCollectionCell, fetchNextDataFor indexPath:IndexPath?)
    func moviesCollectionCell(_ cell:MoviesCollectionCell, didSelect item: SCCMovie)
}

final public class MoviesCollectionCell: UITableViewCell {
    weak var delegate:MoviesCollectionCellDelegate?
    var indexPath:IndexPath?
    @IBOutlet weak var movieCollectionView: MoviesHorizontalCollection! {
        didSet {
            self.configureCollection()
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    
    private(set) var model: [SCCMovie] = []
    
    func set(_ model:[SCCMovie], name:String? = nil) {
        self.titleLabel.text = name
        self.model = model
        self.movieCollectionView.set(model)
        self.movieCollectionView.movieDelegate = self
    }
    
    func update(_ model:[SCCMovie]) {
        self.model = model
        self.movieCollectionView.updete(model)
    }

    private func configureCollection() {
        self.movieCollectionView.remembersLastFocusedIndexPath = true
    }
}

extension MoviesCollectionCell: MoviesHorizontalCollectionDelegate {
    func moviesHorizontal(collection: MoviesHorizontalCollection, didSelect item: SCCMovie) {
        self.delegate?.moviesCollectionCell(self, didSelect: item)
    }
    
    func moviesHorizontalNextPage(_ collection:MoviesHorizontalCollection) {
        self.delegate?.moviesCollectionCell(self, fetchNextDataFor: self.indexPath)
    }
}
