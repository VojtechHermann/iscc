//
//  TVShowCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import XCDYouTubeKit

final class TVShowCoordinator {
    var modelData:MovieViewModel = MovieViewModel() {
        didSet {
            self.detailController?.model = self.modelData
        }
    }
    private var detailController:MovieAndShowViewControllera?
    private var vlcCoordinator:VLCCoordinator?
    
    init(rootViewController: UIViewController, movie: InfoData) {
        self.modelData.rootMovie = movie.getViewModel()
        self.modelData.casts = movie.source?.cast ?? []
        
        let type = movie.source?.infoLabels?.getMediaType()
        self.getRelated(csfdID: movie.source?.services?.csfd, type: type)
        self.getSessionInfo(movie.id, type: type)
        self.getWatched()
        
        let controller = MovieAndShowViewControllera()
        controller.delegate = self
        rootViewController.present(controller, animated: true) {
            controller.model = self.modelData
            if let trailer = self.modelData.rootMovie?.trailer {
                self.playTrailer(url: trailer)
            }
        }
        self.detailController = controller
    }
    
    private func getWatched() {
        guard let movie = self.modelData.rootMovie else { return }
        if self.modelData.type == .tvshow,
           let watchedData = SccWatchedData.getTvShowWatched(by: movie.id) {
            self.modelData.watchedArray = watchedData
        } else if self.modelData.type == .movie,
                  let watchedMovie = SccWatchedData.getWatched(by: movie.id) {
            self.modelData.watchedArray = [watchedMovie]
        }
    }
    
    private func getRelated(csfdID:String?, type:MediaType?) {
        guard let csfdID = csfdID, let type = type else { return }
        let csfdParser = CSFD()
        csfdParser.getRelated(with: csfdID, isTvShow: type == .tvshow) { result in
            switch result {
            case .success(let csfdIds):
                self.getRelatedMovie(csfdDs: csfdIds)
            case .failure(let err):
                print(err)
            }
        }
    }
    
    private func getRelatedMovie(csfdDs:[String]) {
        guard csfdDs.count > 0 else { return }
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.appData.scService.getCsfdMedia(for: csfdDs, type: .all, page: 1, sort: .news) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(var data):
                    data.data?.sort(by: { (left, right) -> Bool in
                        if let leftY = left.source?.infoLabels?.year,
                           let rightY = right.source?.infoLabels?.year {
                            return leftY < rightY
                        }
                        return true
                    })
                    if let data = data.data {
                        var modelData: [RelatedMovieModel] = []
                        for infoData in data {
                            if let model = infoData.getViewModel() {
                                modelData.append(model)
                            }
                        }
                        self.modelData.releatedMovie = modelData
                    }
                case .failure(let err):
                    print(err)
                }
            }
        }
    }
    
    private func getSessionInfo(_ showId: String?, type:MediaType?) {
        guard let type = type,
              type == .tvshow,
              let delegate = UIApplication.shared.delegate as? AppDelegate,
              let showId = showId else { return }
        delegate.appData.scService.getSeries(for: showId, page: 1) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                error.handleError(on: self.detailController)
            case .success(let filterResult):
                guard let data = filterResult.data else { return }
                var modelData: [RelatedMovieModel] = []
                var isEpisodes: Bool = false
                for infoData in data {
                    if let model = infoData.getViewModel() {
                        modelData.append(model)
                        if model.type == .episode {
                            isEpisodes = true
                        }
                    }
                }
                if isEpisodes {
                    self.modelData.episodes = modelData
                } else {
                    self.modelData.seasons = modelData
                    self.searchWatchedAndUpdateEpisodesView()
                }
            }
        }
    }
    
    private func searchWatchedAndUpdateEpisodesView() {
        if self.modelData.watchedArray.isEmpty {
            self.select(season: 0, episode: 0)
            return
        }
        let sorted = self.modelData.watchedArray.sorted { (left, righr) -> Bool in
            if let leftDate = left.lastUpdated,
               let rightDate = righr.lastUpdated {
                return leftDate < rightDate
            }
            return false
        }
        if let lastItem = sorted.last,
           let seasonNumber = lastItem.seasonNumber,
           let episodeNumber = lastItem.episodeNumber {
            self.select(season: seasonNumber - 1, episode: episodeNumber - 1)
        }
    }
    
    private func select(season index: Int, episode: Int, completition:(()->Void)? = nil) {
        guard index < self.modelData.seasons.count else {
            if let completition = completition {
                completition()
            }
            return
        }
        let data = self.modelData.seasons[index]
        let currentSeason = self.modelData.selected.season
        self.modelData.selected = SelectedIndex(season: index, episode: episode)
        if currentSeason != index {
            self.getSessiondEpisodes(data.id, type: data.type, completition: completition)
        } else if let completition = completition {
            completition()
        } else if self.modelData.episodes.count == 0 {
            self.getSessiondEpisodes(data.id, type: data.type, completition: completition)
        }
    }
    
    private func getSessiondEpisodes(_ seasonID: String?, type:MediaType?, completition:(()->Void)? = nil) {
        if let serieID = seasonID,
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.appData.scService.getSeries(for: serieID, page: 1) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let error):
                    error.handleError(on: self.detailController)
                case .success(let filterResult):
                    guard let data = filterResult.data else { return }
                    var modelData: [RelatedMovieModel] = []

                    for infoData in data {
                        if let model = infoData.getViewModel() {
                            modelData.append(model)
                        }
                    }
                    self.modelData.episodes = modelData
                    self.detailController?.reloadEpisode(data:modelData)
                }
                if let completition = completition {
                    completition()
                }
            }
        }
    }
    
    private func playTrailer(url: String) {
        if let range = url.range(of: "=") {
            let youtubeID = String(url[range.upperBound...])
            self.playYoutube(with: youtubeID)
        }
    }
    
    private func playYoutube(with youtubeID:String) {
        XCDYouTubeClient.default().getVideoWithIdentifier(youtubeID) { [weak self] (video: XCDYouTubeVideo?, error: Error?) in
           guard let self = self,
                 let video = video,
                 let videoURL = video.streamURL
           else { return }
            self.detailController?.addVideoPlayer(videoURL)
        }
    }
    
    private func filterWatched(_ itemID:String) -> SccWatched? {
        let watched = self.modelData.watchedArray.filter { data -> Bool in
            if data.episodeID == itemID {
                return true
            }
            if data.seasonID == itemID {
                return true
            }
            return false
        }
        return watched.first
    }
    
    private func update(watched:SccWatched?, for item: RelatedMovieModel) -> SccWatched? {
        guard watched != nil else { return nil }
        var watchedToUpdate = watched
        if item.type == .tvshow {
            watchedToUpdate?.tvShowID = item.id
        } else if item.type == .episode {
            watchedToUpdate?.episodeID = item.id
        } else if item.type == .season {
            watchedToUpdate?.seasonID = item.id
        }
        if let watched = watchedToUpdate {
            self.modelData.watchedArray.append(watched)
        }
        return watchedToUpdate
    }
}


extension TVShowCoordinator: MovieViewControllerDelegate {
    func movieView(_ movieView: MovieAndShowViewControllera, watchedFor item: RelatedMovieModel) -> SccWatched? {
        //get watched by IDs next get watched by trakt ID and update if needed
        if let watched = self.filterWatched(item.id) {
            return watched
        } else if let traktID = item.traktID {
            let watched = SccWatchedData.getWatched(traktID: traktID)
            return self.update(watched: watched, for: item)
        }
        return nil
    }
    func movieView(_ movieView: MovieAndShowViewControllera, showDetailFor movie: RelatedMovieModel) {
        self.detailController?.stopPlayingTrailer()
        
        let _ = TVShowCoordinator(rootViewController: self.detailController!, movie: movie.old)
        
    }
    
    func movieView(_ movieView: MovieAndShowViewControllera, showDetailFor cast: Cast) {
        self.detailController?.stopPlayingTrailer()
        if let actorVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ActorMoviesViewController") as? ActorMoviesViewController {
            actorVC.prepareData(with: cast)
            self.detailController?.present(actorVC, animated: true) {
                
            }
        }
    }
    
    func movieView(_ movieView: MovieAndShowViewControllera, focused item: RelatedMovieModel) {
        if let fanart = item.fanArt {
            self.detailController?.update(fanArt:fanart)
        }
    }
    
    func movieView(_ movieView: MovieAndShowViewControllera, didSelect item: RelatedMovieModel) {
        if item.type == .season,
           let index = self.modelData.seasons.firstIndex(where: { model -> Bool in return model.id == item.id })
        {
            self.select(season: index, episode: 0)
            if let trailerURL = item.trailer {
                self.playTrailer(url: trailerURL)
            }
        }
        if item.type == .episode,
           let index = self.modelData.episodes.firstIndex(where: { model -> Bool in return model.id == item.id }) {
            self.modelData.selected.episode = index
            self.detailController?.stopPlayingTrailer()
            self.playOrContinue()
        }
    }
    
    func movieView(_ movieView: MovieAndShowViewControllera, didPerform action: MovieViewAction) {
        self.detailController?.stopPlayingTrailer()
        if action == .playMovie {
            self.select(season: 0, episode: 0) {
                self.playOrContinue()
            }
        } else if action == .continueMovie {
            self.playOrContinue(true)
        }
    }
}

//MARK: - Play and continue
extension TVShowCoordinator {
    private func playOrContinue(_ isContinue: Bool = false) {
        guard let controller = self.detailController else { return }
        
        if self.vlcCoordinator == nil {
            self.vlcCoordinator = VLCCoordinator()
            self.vlcCoordinator?.delegate = self
        }
        
        if self.modelData.type == .movie {
            self.vlcCoordinator?.set(controller, startAt: 0, isContrinue: isContinue)
        } else {
            let currentSelected = self.modelData.selected
            guard currentSelected.episode < self.modelData.episodes.count else { return }
            self.vlcCoordinator?.set(controller, startAt: currentSelected.episode, isContrinue: isContinue)
        }
    }
}

//MARK: - Watched
extension TVShowCoordinator {
    private func getEmptyWatched() -> SccWatched {
        if self.modelData.type == .movie {
            return SccWatched(isSeen: false, time: 0, lastUpdated: Date(),
                              traktID: self.modelData.rootMovie?.traktID,
                              movieID: self.modelData.rootMovie?.id,
                              tvShowID: nil,
                              sessionNumber: nil, seasonID: nil,
                              episodeNumber: nil, episodeID: nil)
        } else {
            let episodeIndex = self.modelData.selected.episode
            let episodeData = episodeIndex <= self.modelData.episodes.count ? self.modelData.episodes[episodeIndex] : nil
            
            let seasonIndex = self.modelData.selected.season
            let seasonData = seasonIndex <= self.modelData.seasons.count ? self.modelData.seasons[seasonIndex] : nil
            
            let episode = episodeData?.old.source?.infoLabels?.episode ?? 0
            let season = episodeData?.old.source?.infoLabels?.season ?? 0
            let watched = SccWatched(isSeen: false,
                              time: 0,
                              lastUpdated: Date(),
                              traktID: episodeData?.traktID,
                              movieID: nil,
                              tvShowID: self.modelData.rootMovie?.id,
                              sessionNumber: season, seasonID: seasonData?.id,
                              episodeNumber: episode, episodeID: episodeData?.id)
            return watched
        }
    }
}

//MARK: - VLCCoordinatorDelegate
extension TVShowCoordinator: VLCCoordinatorDelegate {
    func coordinator(_ coordinator: VLCCoordinator, update watched: SccWatched) {
        if self.modelData.type == .movie {
            self.modelData.watchedArray = [watched]
            self.detailController?.reloadData()
            return
        }
        var watchedArray = self.modelData.watchedArray
        var isUpdated:Bool = false
        for i in 0...watchedArray.count - 1 {
            if watchedArray[i].seasonNumber == watched.seasonNumber,
               watchedArray[i].episodeNumber == watched.episodeNumber {
                watchedArray[i] = watched
                isUpdated = true
                break
            }
        }
        if isUpdated == false {
            watchedArray.append(watched)
        }
        self.modelData.watchedArray = watchedArray
        self.detailController?.reloadData()
    }

    func numberOfVideos(_ coordinator: VLCCoordinator) -> Int {
        if self.modelData.type == .movie {
            return 1
        }
        return self.modelData.episodes.count
    }

    func coordinator(_ coordinator: VLCCoordinator, data atIndex: Int) -> InfoData {
        if self.modelData.type == .movie, let data = self.modelData.rootMovie?.old {
            return data
        } else if self.modelData.episodes.count > atIndex {
            self.modelData.selected.episode = atIndex
            return self.modelData.episodes[atIndex].old
        }
        return InfoData.empty()
    }

    func coordinator(_ coordinator: VLCCoordinator, watched forData: InfoData) -> SccWatched? {
        if self.modelData.type == .tvshow {
            if let currentWatched = self.modelData.watchedArray.first(where: { $0.episodeID == forData.id }) {
                return currentWatched
            }
        }
        if self.modelData.type == .movie {
            if let currentWatched = self.modelData.watchedArray.first(where: { $0.movieID == forData.id }) {
                return currentWatched
            }
        }

        let watched = self.getEmptyWatched()
        self.modelData.watchedArray.append(watched)
        
        return watched
    }

    func coordinator(_ coordinator: VLCCoordinator, infoPanel forData: InfoData) -> InfoPanelData {
        var panelData = InfoPanelData(title: "", desc: "", posterURL: nil, bannerURL: nil, clearLogoURL: nil)

        if let title = forData.source?.getInfoLabels().title{
            panelData.title = title
        }
        if let desc = forData.source?.getInfoLabels().plot {
            panelData.desc = desc
        }
        if let banner = forData.source?.getInfoLabels().art?.banner,
         !banner.isEmpty {
            panelData.bannerURL = banner
        }
        if let poster = forData.source?.getInfoLabels().art?.poster,
         !poster.isEmpty {
            panelData.posterURL = poster
        }
        if let clearLogo = forData.source?.getInfoLabels().art?.clearlogo,
         !clearLogo.isEmpty {
            panelData.clearLogoURL = clearLogo
        }
        if panelData.posterURL == nil {
            panelData.posterURL = self.modelData.rootMovie?.poster?.absoluteString
        }
        if panelData.bannerURL == nil {
            panelData.bannerURL = self.modelData.rootMovie?.fanArt?.absoluteString
        }
        if panelData.clearLogoURL == nil {
            panelData.clearLogoURL = self.modelData.rootMovie?.logo?.absoluteString
        }
        return panelData
    }
}
