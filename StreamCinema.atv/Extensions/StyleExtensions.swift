//
//  StyleExtensions.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 22.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit


extension UIAlertController {

    func setupSCCStyle() {
        view.tintColor = UIColor.blue
    }
}
