//
//  UITapGesture+Block.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 26.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

import UIKit

public typealias TapAction = () -> Void

extension UITapGestureRecognizer {

    private struct AssociatedKeys {
        static var ActionKey = "ActionKey"
    }

    private class ActionWrapper {
        let action: TapAction
        init(action: @escaping TapAction) {
            self.action = action
        }
    }

    public var onTap: TapAction? {
        set(newValue) {
            removeTarget(self, action:  #selector(performAction))
            var wrapper: ActionWrapper? = nil
            if let newValue = newValue {
                wrapper = ActionWrapper(action: newValue)
                addTarget(self, action: #selector(performAction))
            }

            objc_setAssociatedObject(self, &AssociatedKeys.ActionKey, wrapper, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            guard let wrapper = objc_getAssociatedObject(self, &AssociatedKeys.ActionKey) as? ActionWrapper else {
                return nil
            }

            return wrapper.action
        }
    }

    @objc func performAction() {
        onTap?()
    }
}




