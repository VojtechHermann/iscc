//
//  Movie.swift
//  MovieKit
//
//  Created by Alfian Losari on 11/24/18.
//  Copyright © 2018 Alfian Losari. All rights reserved.
//

import UIKit
import TraktKit

public struct MoviesResponse {
    public let page: Int
    public let totalResults: Int
    public let totalPages: Int
    public let results: [Movie]
}

public class Movie {
    
    public var scId: String?
    public let traktID: String?
    public let title: String?
    public let overview: String?
    public let releaseDate: Int?
    public let voteAverage: String?
    public let genres: [String]?
    public var availableDubLangs: [Language]?
    public var availableSubLangs: [Language]?
    public let videos: MovieVideoResponse?
    public let credits: MovieCreditResponse?
    public var adult: Bool?
    public var posterURL: URL?
    public var bannerURL: URL?
    public var clearLogoURL: URL?
    public var trailerURL: URL?
    public var tvInfo: TvInfo?
    
    public init (model: InfoData) {
        self.scId = model.id
        self.traktID = model.source?.services?.trakt
        let infoLabels = model.source?.getInfoLabels()
        self.title = infoLabels?.title
        self.overview = infoLabels?.plot
        if let year = model.source?.infoLabels?.year {
            self.releaseDate = year
        } else {
            self.releaseDate = nil
        }
        self.voteAverage = model.source?.getRating()
        self.genres = model.source?.infoLabels?.genre
        self.adult = model.source?.adult
        if let poster = infoLabels?.art?.poster, !poster.isEmpty {
            self.posterURL = URL(string: poster)!
        } else {
            self.posterURL = nil
        }
        
        if let banner = infoLabels?.art?.banner, !banner.isEmpty {
            self.bannerURL = URL(string: banner)!
        } else {
            self.bannerURL = nil
        }
        
        if let clearLogo = infoLabels?.art?.clearlogo, !clearLogo.isEmpty {
            self.clearLogoURL = URL(string: clearLogo)!
        } else {
            self.clearLogoURL = nil
        }
        
        if let trailerURL = infoLabels?.trailer {
            self.trailerURL = URL(string: trailerURL)
        } else {
            self.trailerURL = nil
        }
        
        self.availableDubLangs = model.source?.availableStreams?.getAudio()
        self.availableSubLangs = model.source?.availableStreams?.getSubtitles()
        
        self.videos = nil
        self.credits = nil
        self.tvInfo = model.tvInfo
    }
    
    public init(trakt movie:TraktMovie) {
        self.scId = nil
        self.traktID = String(movie.ids.trakt)
        self.title = movie.title
        self.overview = movie.overview
        self.releaseDate = movie.year
        
        if let rating = movie.rating {
            self.voteAverage = "\(rating * 10) %"
        } else {
            self.voteAverage = nil
        }
        self.genres = movie.genres
        self.adult = nil
        self.posterURL = nil
        self.clearLogoURL = nil
        self.bannerURL = nil
        self.trailerURL = movie.trailer
        
        self.availableDubLangs = nil
        self.availableSubLangs = nil
        
        self.videos = nil
        self.credits = nil
        self.tvInfo = nil
    }
    
    public init(trakt tvShow: TraktShow) {
        self.scId = nil
        self.traktID = String(tvShow.ids.trakt)
        self.title = tvShow.title
        self.overview = tvShow.overview
        self.releaseDate = tvShow.year
        if let rating = tvShow.rating {
            self.voteAverage = "\(rating * 10) %"
        } else {
            self.voteAverage = nil
        }
        self.genres = tvShow.genres
        self.adult = nil
        self.posterURL = nil
        self.clearLogoURL = nil
        self.bannerURL = nil
        self.trailerURL = tvShow.trailer
        
        self.availableDubLangs = nil
        self.availableSubLangs = nil
        
        self.videos = nil
        self.credits = nil
        self.tvInfo = nil
    }
    
//    public func getDataFromSCC() -> Bool {
//        if scId != nil {
//            return false
//        }
//        guard let traktID = self.traktID,
//            let appDelegate = UIApplication.shared.delegate as? AppDelegate  else { return false }
//        appDelegate.appData.scService.getMovies(traktID: [traktID], page: 1) { [weak self] result in
//            guard let self = self else { return }
//            switch result {
//            case .success(let resultData):
//                if let value = resultData.data?.first, let infoData = value.source?.getInfoLabels() {
//                    self.scId = value.id
//                    self.adult = value.source?.adult
//                    self.availableDubLangs = value.source?.availableStreams?.getAudio()
//                    self.availableSubLangs = value.source?.availableStreams?.getSubtitles()
//                    if let poster = infoData.art?.poster, !poster.isEmpty {
//                        self.posterURL = URL(string: poster)!
//                    }
//                    if let banner = infoData.art?.banner, !banner.isEmpty {
//                        self.bannerURL = URL(string: banner)!
//                    }
//                    if let clearLogo = infoData.art?.clearlogo, !clearLogo.isEmpty {
//                        self.clearLogoURL = URL(string: clearLogo)!
//                    }
//                    if let trailerURL = infoData.trailer {
//                        self.trailerURL = URL(string: trailerURL)
//                    }
//                    
//                    NotificationCenter.default.post(name: .MovieUpdate, object: self)
//                }
//            case .failure(_):
//                break
//            }
//        }
//        return true
//    }
    
}

public struct MovieGenre: Codable {
    let name: String
}

public struct MovieVideoResponse: Codable {
    public let results: [MovieVideo]
}

public struct MovieVideo: Codable {
    public let id: String
    public let key: String
    public let name: String
    public let site: String
    public let size: Int
    public let type: String
    
    public var youtubeURL: URL? {
        guard site == "YouTube" else {
            return nil
        }
        return URL(string: "https://www.youtube.com/watch?v=\(key)")
    }
}

public struct MovieCreditResponse: Codable {
    public let cast: [MovieCast]
    public let crew: [MovieCrew]
}

public struct MovieCast: Codable {
    public let character: String
    public let name: String
}

public struct MovieCrew: Codable {
    public let id: Int
    public let department: String
    public let job: String
    public let name: String
}
