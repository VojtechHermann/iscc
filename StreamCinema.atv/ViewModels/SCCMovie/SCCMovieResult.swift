//
//  SCCMovieResult.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

struct SCCMovieResult {
    var pagination: Pagination?
    var totalCount: Int?
    var data: [SCCMovie]
    
    init(data: [SCCMovie], pagination: Pagination?, totalCount: Int?) {
        self.data = data
        self.pagination = pagination
        self.totalCount = totalCount
    }
    
    init(_ resultData:FilterResult) {
        self.pagination = resultData.pagination
        self.totalCount = resultData.totalCount
        if var data = resultData.data {
            #if TARGET_OS_TVOS
            if let isDisabled = (UIApplication.shared.delegate as? AppDelegate)?.appData.isDisableAdult,
               isDisabled == true {
                data = data.filter { movie -> Bool in
                    if let adult = movie.source?.infoLabels?.genre,
                       adult.contains(where: { Genere.explicitGenere.contains($0) }) {  
                        return false
                    }
                    return true
                }
            }
            #endif
            let mapedData = data.map { infoData -> SCCMovie in
                return infoData.getSCCMovie()
            }
            self.data = mapedData
        } else {
            self.data = []
        }
    }
    
    init() {
        self.pagination = nil
        self.totalCount = nil
        self.data = []
    }
    
}
