//
//  TabBarItemView.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI
import Combine

struct TabBarItemView: View {

    // MARK: - Constructor properties
    let item: TabBarItem
    @Binding var isFocusedItem: TabBarItem
    let selectedItem: TabBarItem
    @Namespace var tabBarViewNamespace
    let onTap: ((TabBarItem) -> Void)


    private let didUpdatedFocus: PassthroughSubject<Bool, Never> = PassthroughSubject()

    @Namespace var animation

    // MARK: - Private properties
    @State private var isFocused: Bool = false
    private var isSelected: Bool { selectedItem == item }
    private var lastFocusedItem: Bool { isFocusedItem == item }

    private var defaultColor: Color {
        Color(sharedColor: .defaultGreen)
    }

    @State private var timer: Timer?

    var body: some View {


            Button {
                onTap(item)
            } label: {

                VStack {

                    if item.isFirst {
                        Spacer()
                    }

                    if item == .trakt {
                        Image(item.image)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 40, height: 40)
                            .scaleEffect(1.4)
                            .scaleEffect(isFocused ? 1.2 : 1)

                    } else {
                        Image(systemName: item.systemImage)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 40, height: 40)
                            .scaleEffect(isFocused ? 1.2 : 1)
                    }

                    if item.isLast {
                        Spacer()
                    }

                }
                .foregroundColor(
                    lastFocusedItem ? Color.white : defaultColor
                )
                .matchedGeometryEffect(id: item.rawValue, in: animation)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .animation(.default, value: lastFocusedItem)
            }
//            .padding(.all, 9)
            .onReceive(didUpdatedFocus, perform: { (isFocused) in
                if isFocused {
                    timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { _ in
                        DispatchQueue.main.async {
                            onTap(item)
                        }
                     }
                } else {
                    timer?.invalidate()
                    timer = nil
                }
                self.isFocused = isFocused
            })
            .buttonStyle(TabBarButtonStyle(item: item, focusedItem: $isFocusedItem, didUpdatedFocus: didUpdatedFocus, tabBarViewNamespace: _tabBarViewNamespace))

    }
}


private struct TabBarButtonStyle: ButtonStyle {

    let item: TabBarItem
    @Binding var focusedItem: TabBarItem
    let didUpdatedFocus: PassthroughSubject<Bool, Never>
    @Namespace var tabBarViewNamespace

    func makeBody(configuration: Configuration) -> some View {

        configuration.label
            .modifier(FocusModifier(item: item, focusedItem: $focusedItem,
                                    didUpdatedFocus: didUpdatedFocus, tabBarViewNamespace: _tabBarViewNamespace))
    }
}

private struct FocusModifier: ViewModifier {

    @Environment(\.isFocused) var isFocused: Bool

    let item: TabBarItem
    @Binding var focusedItem: TabBarItem
    let didUpdatedFocus: PassthroughSubject<Bool, Never>
    @Namespace var tabBarViewNamespace


    func body(content: Content) -> some View {
        #if os(iOS)
            return content
        #elseif os(tvOS)

        return GeometryReader { proxy in
                content
                .focusable()
//                .scaleEffect(isFocused ? 1.3 : 1)
                .animation(.spring(), value: isFocused)
                .onChange(of: isFocused, perform: { isFocused in
                    DispatchQueue.main.async {
                        guard isFocused else { return }
                        focusedItem = item
                    }

                    self.didUpdatedFocus.send(isFocused)
                })
            }
        #else
            return content
        #endif
    }

}

